<?php

namespace Team1\Instagram\Setup;

use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

/**
 * Class InstallSchema
 * @package Team1\Instagram\Setup
 */
class InstallSchema implements InstallSchemaInterface
{
    /**
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     * @throws \Zend_Db_Exception
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;
        $installer->startSetup();

        /**
         * Create table 'Team1_Instagram'
         */
        $tableName = 'sm_images';
        $table = $installer->getConnection()->newTable(
            $installer->getTable($tableName)
        )->addColumn(
            'entity_id',
            Table::TYPE_INTEGER,
            null,
            [
                'nullable' => false,
                'unsigned' => true,
                'identity' => true,
                'primary' => true
            ],
            'Entity Id'
        )->addColumn(
            'name',
            Table::TYPE_TEXT,
            1024,
            [
                'nullable' => false,
            ],
            'Image name'
        )->addColumn(
            'link',
            Table::TYPE_TEXT,
            2048,
            [
                'nullable' => false,
            ],
            'Image link'
        )->addColumn(
            'description',
            Table::TYPE_TEXT,
            2048,
            [
                'nullable' => false,
            ],
            'Image description'
        )->addColumn(
            'created_at',
            Table::TYPE_TIMESTAMP,
            null,
            [
                'nullable' => false,
                'default' => TABLE::TIMESTAMP_INIT
            ],
            'Created at'
        )->addColumn(
            'updated_at',
            Table::TYPE_TIMESTAMP,
            null,
            [
                'nullable' => false,
                'default' => TABLE::TIMESTAMP_INIT_UPDATE
            ],
            'Updated at'
        )->addColumn(
            'is_active',
            Table::TYPE_BOOLEAN,
            null,
            [
                'nullable' => false,
            ],
            'Is active'
        )->setComment(
            'Team one sample table'
        );
        $installer->getConnection()->createTable($table);

        $installer->endSetup();
    }
}
