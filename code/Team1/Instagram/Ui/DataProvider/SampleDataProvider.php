<?php

namespace Team1\Instagram\Ui\DataProvider;

use Magento\Framework\App\Request\DataPersistorInterface;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\View\Element\UiComponent\DataProvider\DataProviderInterface;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Ui\DataProvider\AbstractDataProvider;
use Team1\Instagram\Model\ResourceModel\OneSample\CollectionFactory;
use Team1\Instagram\Model\Sample\ImageFileUploader;

/**
 * Class OverlayDataProvider
 * @package Team1\Instagram\Ui\DataProvider
 */
class SampleDataProvider extends AbstractDataProvider implements DataProviderInterface
{
    /**
     * @var RequestInterface
     */
    private $request;

    /**
     * @var DataPersistorInterface
     */
    private $dataPersistor;

    /**
     * @var ImageFileUploader
     */
    private $imageFileUploader;

    /**
     * @var StoreManagerInterface
     */
    private $storeManager;

    /**
     * SampleDataProvider constructor.
     * @param $name
     * @param $primaryFieldName
     * @param $requestFieldName
     * @param CollectionFactory $collectionFactory
     * @param RequestInterface $request
     * @param DataPersistorInterface $dataPersistor
     * @param ImageFileUploader $imageFileUploader
     * @param StoreManagerInterface $storeManager
     * @param array $meta
     * @param array $data
     */
    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        CollectionFactory $collectionFactory,
        RequestInterface $request,
        DataPersistorInterface $dataPersistor,
        ImageFileUploader $imageFileUploader,
        StoreManagerInterface $storeManager,
        array $meta = [],
        array $data = []
    ) {
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
        $this->collection = $collectionFactory->create();
        $this->request = $request;
        $this->dataPersistor = $dataPersistor;
        $this->imageFileUploader = $imageFileUploader;
        $this->storeManager = $storeManager;
    }

    /**
     * {@inheritdoc}
     */
    public function getData()
    {
        $data = [];
        $dataFromForm = $this->dataPersistor->get('one_sample');
        if (!empty($dataFromForm)) {
            $data[$dataFromForm['id']] = $dataFromForm;
            $this->dataPersistor->clear('one_sample');
        } else {
            $id = $this->request->getParam($this->getRequestFieldName());
            /** @var \Team1\Instagram\Model\OneSample $oneSample */
            foreach ($this->getCollection()->getItems() as $oneSample) {
                if ($id == $oneSample->getEntityId()) {
                    $oneSample->setData('link', [
                            [
                                'url' => $this->imageFileUploader->getMediaUrl($oneSample->getLink()),
                                'file' => @$oneSample->getLink()
                            ]
                        ]);
                }
                $data[$id] = $oneSample->getData();
            }
        }
        return $data;
    }
}
