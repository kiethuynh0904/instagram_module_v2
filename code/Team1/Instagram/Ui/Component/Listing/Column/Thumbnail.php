<?php
namespace Team1\Instagram\Ui\Component\Listing\Column;

use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\UrlInterface;
use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Ui\Component\Listing\Columns\Column;
use Team1\Instagram\Model\Sample\ImageFileUploader;

/**
 * Class Thumbnail
 * @package Team1\Instagram\Ui\Component\Listing\Column
 */
class Thumbnail extends Column
{
    /**
     * @var UrlInterface
     */
    protected $urlBuilder;

    /**
     * @var ImageFileUploader
     */
    protected $imageFileUploader;

    /**
     * Thumbnail constructor.
     * @param UrlInterface $urlBuilder
     * @param ImageFileUploader $imageFileUploader
     * @param ContextInterface $context
     * @param UiComponentFactory $uiComponentFactory
     * @param array $components
     * @param array $data
     */
    public function __construct(
        UrlInterface $urlBuilder,
        ImageFileUploader $imageFileUploader,
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        array $components = [],
        array $data = []
    ) {
        $this->urlBuilder = $urlBuilder;
        $this->imageFileUploader = $imageFileUploader;
        parent::__construct($context, $uiComponentFactory, $components, $data);
    }

    /**
     * @param array $dataSource
     * @return array
     * @throws NoSuchEntityException
     */
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            $fieldName = $this->getData('name');
            foreach ($dataSource['data']['items'] as & $item) {
                $imageFile = $item['link'];
                $imageUrl = $this->imageFileUploader->getMediaUrl($imageFile);
                $item[$fieldName . '_src'] = $imageUrl;
                $item[$fieldName . '_alt'] = $item['name'];
                $item[$fieldName . '_link'] = $this->urlBuilder->getUrl(
                    'slide/one_sample/edit',
                    ['entity_id' => $item['entity_id']]
                );
                $item[$fieldName . '_orig_src'] = $imageUrl;
            }
        }

        return $dataSource;
    }
}
