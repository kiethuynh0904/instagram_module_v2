<?php
namespace Team1\Instagram\Model\ResourceModel\OneSample;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use Team1\Instagram\Model\OneSample;
use Team1\Instagram\Model\ResourceModel\OneSample as OneSampleResource;


/**
 * Class Collection
 * @package Team1\Instagram\Model\ResourceModel\OneSample
 */
class Collection extends AbstractCollection
{
    /**
     * @var string
     */
    protected $_idFieldName = 'entity_id';

    /**
     * Collection initialisation
     */
    protected function _construct()
    {
        $this->_init(OneSample::class, OneSampleResource::class);
    }
}
