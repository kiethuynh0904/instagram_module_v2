<?php
namespace Team1\Instagram\Model\ResourceModel;

use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Model\ResourceModel\Db\AbstractDb;
use Team1\Instagram\Api\Data\OneSampleInterface;

/**
 * Class OneSample
 * @package Team1\Instagram\Model\ResourceModel
 */
class OneSample extends AbstractDb
{
    /**
     * Resource initialisation
     * @codingStandardsIgnoreStart
     */
    protected function _construct()
    {
        $this->_init('sm_images', OneSampleInterface::ENTITY_ID);
    }

    /**
     * @param int $id
     * @return array
     * @throws LocalizedException
     */
    public function getById($id)
    {
        $select = $this->getConnection()->select()
            ->from($this->getMainTable())
            ->where(OneSampleInterface::ENTITY_ID . ' = ?', $id)
            ->limit(1);
        return $this->getConnection()->fetchRow($select);
    }

    /**
     * @param $id
     * @throws LocalizedException
     */
    public function deleteById($id)
    {
        $this->getConnection()->delete(
            $this->getMainTable(),
            ['entity_id = ?' => $id]
        );
    }
}
