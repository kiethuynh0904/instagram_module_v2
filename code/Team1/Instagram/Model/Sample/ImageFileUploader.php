<?php

namespace Team1\Instagram\Model\Sample;

/**
 * Class ImageFileUploader
 * @package Team1\Instagram\Model\Sample
 */
class ImageFileUploader extends FileUploader
{
    /**
     * Get allowed file extensions
     *
     * @return string[]
     */
    public function getAllowedExtensions()
    {
        return ['jpg', 'jpeg', 'gif', 'png'];
    }
}
