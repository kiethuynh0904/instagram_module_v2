<?php

namespace Team1\Instagram\Model;

use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use Team1\Instagram\Api\Data\OneSampleInterface;
use Team1\Instagram\Api\Data\OneSampleInterfaceFactory;
use Team1\Instagram\Api\OneSampleRepositoryInterface;
use Team1\Instagram\Model\ResourceModel\OneSample as OneSampleResourceModel;

/**
 * Class OneSampleRepository
 * @package Team1\Instagram\Model
 */
class OneSampleRepository implements OneSampleRepositoryInterface
{
    /**
     * @var OneSampleInterfaceFactory
     */
    protected $oneSampleInterfaceFactory;

    /**
     * @var OneSampleResourceModel
     */
    protected $oneSampleResourceModel;

    /**
     * OneSampleRepository constructor.
     * @param OneSampleInterfaceFactory $oneSampleInterfaceFactory
     * @param OneSampleResourceModel $oneSampleResourceModel
     */
    public function __construct(
        OneSampleInterfaceFactory $oneSampleInterfaceFactory,
        OneSampleResourceModel $oneSampleResourceModel
    ) {
        $this->oneSampleInterfaceFactory = $oneSampleInterfaceFactory;
        $this->oneSampleResourceModel = $oneSampleResourceModel;
    }

    /**
     * @inheritdoc
     */
    public function save(OneSampleInterface $oneSample)
    {
        try {
            $this->oneSampleResourceModel->save($oneSample);
        } catch (\Exception $e) {
            throw new CouldNotSaveException(__($e->getMessage()));
        }
        return $oneSample;
    }

    /**
     * @inheritdoc
     */
    public function get($oneSampleId)
    {
        /** @var OneSampleInterface $oneSampleModel */
        $oneSampleModel = $this->oneSampleInterfaceFactory->create();
        $this->oneSampleResourceModel->load($oneSampleModel, $oneSampleId);
        if (!$oneSampleModel->getId()) {
            throw new NoSuchEntityException(__('One sample object with id "%1" does not exist.', $oneSampleId));
        }

        return $oneSampleModel;
    }
}
