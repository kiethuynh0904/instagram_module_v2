<?php

namespace Team1\Instagram\Controller\Adminhtml\One\Sample;

use Magento\Backend\App\Action\Context;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Registry;
use Magento\Framework\View\Result\PageFactory;
use Team1\Instagram\Controller\Adminhtml\One\Common as CommonController;
use Team1\Instagram\Model\Sample\ImageFileUploader;

/**
 * Class UploadImage
 * @package Team1\Instagram\Controller\Adminhtml\One\Sample
 */
class UploadImage extends CommonController
{
    /**
     * @var ImageFileUploader
     */
    private $imageFileUploader;

    public function __construct(
        ImageFileUploader $imageFileUploader,
        Registry $registry,
        PageFactory $resultPageFactory,
        Context $context
    ) {
        $this->imageFileUploader = $imageFileUploader;
        parent::__construct($registry, $resultPageFactory, $context);
    }

    /**
     * Image upload action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $imageId = $this->_request->getParam('param_name', 'image');
        $result = $this->imageFileUploader->saveFileToMediaFolder($imageId);
        return $this->resultFactory->create(ResultFactory::TYPE_JSON)->setData($result);
    }
}
