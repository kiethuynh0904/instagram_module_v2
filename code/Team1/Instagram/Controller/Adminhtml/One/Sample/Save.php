<?php

namespace Team1\Instagram\Controller\Adminhtml\One\Sample;

use Magento\Backend\App\Action\Context;
use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\Result\Redirect;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Registry;
use Magento\Framework\View\Result\PageFactory;
use Team1\Instagram\Api\Data\OneSampleInterface;
use Team1\Instagram\Controller\Adminhtml\One\Common as CommonController;
use Team1\Instagram\Model\OneSampleFactory;
use Team1\Instagram\Model\OneSampleRepository;
use Team1\Instagram\Model\ResourceModel\OneSample as OneSampleResource;

/**
 * Class Save
 * @package Team1\Instagram\Controller\Adminhtml\Url\Redirect
 */
class Save extends CommonController
{
    /**
     * @var DataObjectHelper
     */
    protected $dataObjectHelper;

    /**
     * @var OneSampleFactory
     */
    protected $oneSampleModelFactory;

    /**
     * @var OneSampleRepository
     */
    protected $oneSampleRepository;

    /**
     * @var OneSampleResource
     */
    protected $oneSampleResource;

    /**
     * Save constructor.
     * @param DataObjectHelper $dataObjectHelper
     * @param OneSampleFactory $oneSampleModelFactory
     * @param OneSampleRepository $oneSampleRepository
     * @param OneSampleResource $oneSampleResource
     * @param Registry $registry
     * @param PageFactory $resultPageFactory
     * @param Context $context
     */
    public function __construct(
        DataObjectHelper $dataObjectHelper,
        OneSampleFactory $oneSampleModelFactory,
        OneSampleRepository $oneSampleRepository,
        OneSampleResource $oneSampleResource,
        Registry $registry,
        PageFactory $resultPageFactory,
        Context $context
    ) {
        $this->dataObjectHelper = $dataObjectHelper;
        $this->oneSampleModelFactory = $oneSampleModelFactory;
        $this->oneSampleRepository = $oneSampleRepository;
        $this->oneSampleResource = $oneSampleResource;

        parent::__construct($registry, $resultPageFactory, $context);
    }

    /**
     * @return ResponseInterface|Redirect|ResultInterface
     */
    public function execute()
    {
        $data = $this->getRequest()->getParams();
        $resultRedirect = $this->resultRedirectFactory->create();
        if (!$data) {
            $this->messageManager->addErrorMessage(__('There is no data to save'));
        } else {
            try {
                $this->performSave($data);
                $this->messageManager->addSuccessMessage(__('One sample object save success'));
                if ($this->getRequest()->getParam('back')) {
                    return $resultRedirect->setPath('*/*/edit', ['entity_id' => $data[OneSampleInterface::ENTITY_ID], '_current' => true]);
                }
            } catch (\Exception $exception) {
                $this->messageManager->addErrorMessage(__($exception->getMessage()));
            }
        }
        return $resultRedirect->setPath('*/*');
    }

    /**
     * @param array $data
     * @return OneSampleInterface
     * @throws CouldNotSaveException
     * @throws LocalizedException
     */
    private function performSave($data)
    {
        $id = !empty($data['entity_id']) ? $data['entity_id'] : null;

        $oneSampleObject = $id
            ? $this->oneSampleRepository->get($id)
            : $this->oneSampleModelFactory->create();

        $this->dataObjectHelper->populateWithArray(
            $oneSampleObject,
            $data,
            OneSampleInterface::class
        );

        if (isset($data['link'][0]['file'])) {
            $oneSampleObject->setLink($data['link'][0]['file']);
        }

        $oneSampleObject->setEntityId($id);
        $oneSample = $this->oneSampleRepository->save($oneSampleObject);
        return $oneSample;
    }
}
