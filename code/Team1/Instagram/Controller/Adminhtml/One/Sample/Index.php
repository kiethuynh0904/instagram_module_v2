<?php

namespace Team1\Instagram\Controller\Adminhtml\One\Sample;

use Magento\Backend\Model\View\Result\Page;

/**
 * Class Index
 * @package Team1\Instagram\Controller\Adminhtml\One\Sample
 */
class Index extends \Team1\Instagram\Controller\Adminhtml\One\Common
{
    /**
     * Index action
     *
     * @return Page
     */
    public function execute()
    {
        /** @var Page $resultPage */
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu(self::ADMIN_RESOURCE);
        $resultPage->getConfig()->getTitle()->prepend(__('Listing Images'));
        return $resultPage;
    }
}
