<?php

namespace Team1\Instagram\Controller\Adminhtml\One\Sample;

use Magento\Backend\App\Action\Context;
use Magento\Backend\Model\View\Result\Page;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\Result\Redirect;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Registry;
use Magento\Framework\View\Result\PageFactory;
use Team1\Instagram\Controller\Adminhtml\One\Common as CommonController;
use Team1\Instagram\Model\ResourceModel\OneSample as OneSampleResource;

/**
 * Class Edit
 * @package Team1\Instagram\Controller\Adminhtml\One\Sample
 */
class Edit extends CommonController
{
    /**
     * @var OneSampleResource
     */
    protected $oneSampleResource;

    /**
     * Edit constructor.
     * @param OneSampleResource $oneSampleResource
     * @param Registry $registry
     * @param PageFactory $resultPageFactory
     * @param Context $context
     */
    public function __construct(
        OneSampleResource $oneSampleResource,
        Registry $registry,
        PageFactory $resultPageFactory,
        Context $context
    ) {
        $this->oneSampleResource = $oneSampleResource;
        parent::__construct($registry, $resultPageFactory, $context);
    }

    /**
     * @return Page|ResponseInterface|Redirect|ResultInterface
     * @throws LocalizedException
     */
    public function execute()
    {
        $id = $this->getRequest()->getParam('entity_id');
        if ($id) {
            $urlRedirectData = $this->oneSampleResource->getById($id);
            if (empty($urlRedirectData)) {
                $this->messageManager->addErrorMessage(__('The requested raw does not exist'));
                $resultRedirect = $this->resultRedirectFactory->create();
                return $resultRedirect->setPath('*/*');
            } else {
                $this->registry->register('current_one_sample_id', $id);
            }
        }

        /** @var Page $resultPage */
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu(self::ADMIN_RESOURCE);
        $resultPage->getConfig()->getTitle()->prepend(__('Image'));
        return $resultPage;
    }
}
