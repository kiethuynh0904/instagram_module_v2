<?php

namespace Team1\Instagram\Controller\Adminhtml\One\Sample;

use Magento\Backend\App\Action\Context;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Registry;
use Magento\Framework\View\Result\PageFactory;
use Team1\Instagram\Api\Data\OneSampleInterface;
use Team1\Instagram\Controller\Adminhtml\One\Common as CommonController;
use Team1\Instagram\Model\ResourceModel\OneSample as OneSampleResource;

/**
 * Class Delete
 * @package Team1\Instagram\Controller\Adminhtml\One\Sample
 */
class Delete extends CommonController
{
    /**
     * @var OneSampleResource
     */
    protected $oneSampleResource;

    /**
     * Delete constructor.
     * @param OneSampleResource $oneSampleResource
     * @param Registry $registry
     * @param PageFactory $resultPageFactory
     * @param Context $context
     */
    public function __construct(
        OneSampleResource $oneSampleResource,
        Registry $registry,
        PageFactory $resultPageFactory,
        Context $context
    ) {
        $this->oneSampleResource = $oneSampleResource;
        parent::__construct($registry, $resultPageFactory, $context);
    }

    /**
     * @return ResponseInterface|ResultInterface|void
     */
    public function execute()
    {
        $id = $this->getRequest()->getParam(OneSampleInterface::ENTITY_ID);
        if ($id) {
            try {
                $this->oneSampleResource->deleteById($id);
                $this->messageManager->addSuccessMessage(__('Delete image success'));
            } catch (\Exception $e) {
                $this->messageManager->addErrorMessage($e->getMessage());
            }
        }
        $this->_redirect('*/*/');
    }
}
