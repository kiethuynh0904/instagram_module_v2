<?php

namespace Team1\Instagram\Api;

use Magento\Framework\Exception\LocalizedException;
use Team1\Instagram\Api\Data\OneSampleInterface;

/**
 * Interface OneSampleRepositoryInterface
 * @package Team1\Instagram\Api
 */
interface OneSampleRepositoryInterface
{
    /**
     * @param OneSampleInterface $oneSample
     * @return OneSampleInterface
     */
    public function save(OneSampleInterface $oneSample);

    /**
     * @param int $oneSampleId
     * @return OneSampleInterface
     * @throws LocalizedException
     */
    public function get($oneSampleId);
}
