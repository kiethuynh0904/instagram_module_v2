<?php

namespace Team1\Instagram\Api\Data;

/**
 * Interface OneSampleInterface
 * @package Team1\Instagram\Api\Data
 */
interface OneSampleInterface
{
    const ENTITY_ID = 'entity_id';

    const LINK = 'link';

    const DESCRIPTION = 'description';

    const NAME = 'name';

    const CREATED_AT = 'created_at';

    const UPDATED_AT = 'updated_at';

    const IS_ACTIVE = 'is_active';

    /**
     * Get entity id
     *
     * @return string
     */
    public function getEntityId();

    /**
     * Set entity id
     *
     * @param int $entityId
     * @return $this
     */
    public function setEntityId($entityId);

    /**
     * Get image link
     *
     * @return string
     */
    public function getLink();

    /**
     * Set image link
     *
     * @param string $link
     * @return $this
     */
    public function setLink($link);

    /**
     * Get image name
     *
     * @return string
     */
    public function getName();

    /**
     * Set image name
     *
     * @param string $name
     * @return $this
     */
    public function setName($name);

    /**
     * Get image description
     *
     * @return string
     */
    public function getDescription();

    /**
     * Set image description
     *
     * @param string $description
     * @return $this
     */
    public function setDescription($description);

    /**
     * Get created at
     *
     * @return string
     */
    public function getCreatedAt();

    /**
     * Set created at
     *
     * @param string $createdAt
     * @return $this
     */
    public function setCreatedAt($createdAt);

    /**
     * Get updated at
     *
     * @return string
     */
    public function getUpdatedAt();

    /**
     * Set updated at
     *
     * @param string $updatedAt
     * @return $this
     */
    public function setUpdatedAt($updatedAt);

    /**
     * Get is active
     *
     * @return boolean
     */
    public function getIsActive();

    /**
     * Set is active
     *
     * @param boolean $isActive
     * @return $this
     */
    public function setIsActive($isActive);
}
