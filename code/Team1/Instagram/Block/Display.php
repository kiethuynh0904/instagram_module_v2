<?php

namespace Team1\Instagram\Block;

use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use Magento\Framework\View\Result\PageFactory;
use Team1\Instagram\Model\ResourceModel\OneSample\CollectionFactory;
class Display extends Template
{
    protected $_pageFactory;
    protected $_gridCollectionFactory;

    public function __construct(Context $context, PageFactory $pageFactory, CollectionFactory $gridCollectionFactory)
    {
//        $this->_pageFactory = $pageFactory;
        $this->_gridCollectionFactory = $gridCollectionFactory;
        parent::__construct($context);
    }

    public function getImage()
    {
        $gridCollection = $this->_gridCollectionFactory->create();
        return $gridCollection;
    }
    public function sayHello()
    {
        return __('Hello World');
    }
}